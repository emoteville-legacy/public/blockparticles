## Block Particles

[![Join us on Discord](https://img.shields.io/discord/182615261403283459.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.badbones69.com)

[![Block Particle's Stargazers](https://img.shields.io/github/stars/Crazy-Crew/Block-Particles?label=stars&logo=github)](https://github.com/Crazy-Crew/Block-Particles/stargazers)
[![Block Particle's Forks](https://img.shields.io/github/forks/Crazy-Crew/Block-Particles?label=forks&logo=github)](https://github.com/Crazy-Crew/Block-Particles/network/members)
[![Block Particle's Watchers](https://img.shields.io/github/watchers/Crazy-Crew/Block-Particles?label=watchers&logo=github)](https://github.com/Crazy-Crew/Block-Particles/watchers)

Block Particles is a plugin that adds particles to your blocks!

## Contact
[![Join us on Discord](https://img.shields.io/discord/182615261403283459.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.badbones69.com)

Join us on [Discord](https://discord.badbones69.com)

## Downloads
[![Build Status](https://jenkins.badbones69.com/view/Stable/job/Block-Particles/badge/icon)](https://jenkins.badbones69.com/view/Stable/job/Block-Particles/)

Downloads can be obtained from the [spigot page](https://www.spigotmc.org/resources/block-particles.13877/) or the [Jenkins](https://jenkins.badbones69.com/view/Stable/job/Block-Particles/).

## Plugin Data
[![bStats Graph Data](https://bstats.org/signatures/bukkit/BlockParticles.svg)](https://bstats.org/signatures/bukkit/BlockParticles)

## API
In Progress.

### Dependency Information

#### Maven
In Progress.

#### Gradle
In Progress.

## Working with Block Particles.

#### Contributing
Fork the project & open a pull request.

#### Compiling
Clone the project & run the shadowJar task.
